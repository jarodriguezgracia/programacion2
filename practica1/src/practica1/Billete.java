package practica1;


import java.util.Calendar;
import java.util.GregorianCalendar;


public class Billete {
	
	private String estacionInicial, estacionDestino, nombreViajero;
	private double precioFinal;
	private Calendar fechaViaje;
	private final static int PRECIO_BASE_EUROS = 10;
	private final static int INCREMENTO_FIN_SEMANA = PRECIO_BASE_EUROS + PRECIO_BASE_EUROS*1;
	private final static int MAXIMA_EDAD_DESCUENTOS = 14;
	private final static int MINIMO_EDAD_DESCUENTOS = 0;
	
	private final static double PRECIO_ESPECIAL = 5.0;
	
	private final static double DESCUENTO_VEINTEPORCIENTO_FAMILIA_NUMEROSA = PRECIO_BASE_EUROS * 0.2;
	private final static double INCREMENTO_CINCUENTAPORCIENTO_LUNES_ALTA_DEMANDA = PRECIO_BASE_EUROS * 0.5;
	private final static double INCREMENTO_OCHO_EUROS_BILLETE_FLEXIBLE = 8.0;

	private static Calendar FECHA_INICIO_PROMOCION_LANZAMIENTO = new GregorianCalendar(2020, Calendar.JANUARY, 27);
	private static Calendar FECHA_FIN_PROMOCION_LANZAMIENTO = new GregorianCalendar(2020, Calendar.FEBRUARY, 5);
	
	
	
	public Billete(String estacionInicial, String estacionDestino, String nombreViajero) {
		this.estacionInicial = estacionInicial;
		this.estacionDestino = estacionDestino;
		this.nombreViajero = nombreViajero;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Billete other = (Billete) obj;
		if (estacionDestino == null) {
			if (other.estacionDestino != null)
				return false;
		} else if (!estacionDestino.equals(other.estacionDestino))
			return false;
		if (estacionInicial == null) {
			if (other.estacionInicial != null)
				return false;
		} else if (!estacionInicial.equals(other.estacionInicial))
			return false;
		if (fechaViaje == null) {
			if (other.fechaViaje != null)
				return false;
		} else if (!fechaViaje.equals(other.fechaViaje))
			return false;
		if (nombreViajero == null) {
			if (other.nombreViajero != null)
				return false;
		} else if (!nombreViajero.equals(other.nombreViajero))
			return false;
		if (Double.doubleToLongBits(precioFinal) != Double.doubleToLongBits(other.precioFinal))
			return false;
		return true;
	}


	public void calcularPrecioFinal(Calendar fechaViaje, int edad, boolean esFamiliaNumerosa, boolean quiereBilleteFlexible){
		this.fechaViaje = fechaViaje;
	
		if(noTieneDescuentoEdad(edad)) { //!
			comprobarDescuentosDias();
			comprobarDescuentoFamiliaNumerosa(esFamiliaNumerosa);
		}
		quiereBilleteFlexible(quiereBilleteFlexible);
	}


	private void quiereBilleteFlexible(boolean quiereBilleteFlexible) { //nombre mas descriptivo
		if(quiereBilleteFlexible == true) {
			ajustarPrecioFinal(Billete.INCREMENTO_OCHO_EUROS_BILLETE_FLEXIBLE);
		}
	}


	private void comprobarDescuentoFamiliaNumerosa(boolean esFamiliaNumerosa) {
		if(esFamiliaNumerosa == true) {
			ajustarPrecioFinal(Billete.DESCUENTO_VEINTEPORCIENTO_FAMILIA_NUMEROSA);
		}
	}


	private void ajustarPrecioFinal(double precio) {
	
		
		if(precio == Billete.PRECIO_ESPECIAL) {
			this.precioFinal = Billete.PRECIO_ESPECIAL;
		}
		
		if(precio == Billete.INCREMENTO_CINCUENTAPORCIENTO_LUNES_ALTA_DEMANDA) {
			this.precioFinal = Billete.PRECIO_BASE_EUROS + Billete.INCREMENTO_CINCUENTAPORCIENTO_LUNES_ALTA_DEMANDA;
		}else if(precio == Billete.INCREMENTO_FIN_SEMANA) {
			this.precioFinal = Billete.INCREMENTO_FIN_SEMANA;
		}else if(precio == Billete.PRECIO_BASE_EUROS) {
			this.precioFinal = Billete.PRECIO_BASE_EUROS;
		}
		
		if(precio == Billete.DESCUENTO_VEINTEPORCIENTO_FAMILIA_NUMEROSA) {
			this.precioFinal = this.precioFinal - Billete.DESCUENTO_VEINTEPORCIENTO_FAMILIA_NUMEROSA;
		}
		if(precio == Billete.INCREMENTO_OCHO_EUROS_BILLETE_FLEXIBLE) {
			this.precioFinal = this.precioFinal + Billete.INCREMENTO_OCHO_EUROS_BILLETE_FLEXIBLE;
		}
	}


	private void comprobarDescuentosDias() { // dividir en 3 funciones de incremento decremento y normal
		
		
		if(this.fechaViaje.after(Billete.FECHA_INICIO_PROMOCION_LANZAMIENTO) &&
				this.fechaViaje.before(Billete.FECHA_FIN_PROMOCION_LANZAMIENTO)) {
			ajustarPrecioFinal(Billete.PRECIO_ESPECIAL);
		}else if(this.fechaViaje.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY) {
			ajustarPrecioFinal(Billete.INCREMENTO_CINCUENTAPORCIENTO_LUNES_ALTA_DEMANDA);
		}else if(this.fechaViaje.get(Calendar.DAY_OF_WEEK) == (Calendar.SUNDAY) ||
				this.fechaViaje.get(Calendar.DAY_OF_WEEK) == (Calendar.FRIDAY) ||
				this.fechaViaje.get(Calendar.DAY_OF_WEEK) == (Calendar.SATURDAY)) {
			ajustarPrecioFinal(Billete.INCREMENTO_FIN_SEMANA);
		}else {
			ajustarPrecioFinal(Billete.PRECIO_BASE_EUROS);
		}
	}


	private boolean noTieneDescuentoEdad(int edad) {
		if(edad < getEdadMaximaConDescuento() && edad > MINIMO_EDAD_DESCUENTOS) {
			ajustarPrecioFinal(Billete.PRECIO_ESPECIAL);
			return false;
		}
		return true;
	}


	@Override
	public String toString() {
		return "Billete:" + "\n"
				+ this.estacionInicial + " -- " + this.estacionDestino + " a las " +
				this.fechaViaje.get(Calendar.HOUR_OF_DAY) + ":" + this.fechaViaje.get(Calendar.MINUTE) + " el " + this.fechaViaje.get(Calendar.DATE) + "/" 
				+ empezarMesesEnUno() + "/" + this.fechaViaje.get(Calendar.YEAR) + "."
				+ "\n" + "Precio Base: " + Billete.PRECIO_BASE_EUROS + "�" + "\n" + "Precio Final: " + this.precioFinal + "�";
		
	}


	private int empezarMesesEnUno() {
		return (this.fechaViaje.get(Calendar.MONTH)+1); //formateo de cadena como cuando pido datos.
	}


	public static int getEdadMaximaConDescuento() {
		return MAXIMA_EDAD_DESCUENTOS;
	}

	
	
	
	
	
	
	
}

package practica1;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

public class GestionBilletes {

	private static final String FORMATO_FECHA = "dd-MM-yyyy HH:mm";
	private static final String SI_MINUSCULA = "si";
	private static final String SI = "SI";

	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		
		System.out.println("Introduce nombre del viajero");
		String nombreViajero = reader.nextLine();
		Billete billete = new Billete("Teruel","Madrid",nombreViajero);
		
	
		int edad;
		Date fecha;
		boolean esfamiliaNumerosa = false, tieneBilleteFlexible = false;
		String respuestaBilleteFlexible, textoFecha;
		
		System.out.println("Introduce edad del pasajero");
		edad = reader.nextInt();
		reader.nextLine();
		System.out.println("Introduce la fecha con formato dd-MM-yyyy HH:mm");
		textoFecha = reader.nextLine();
		fecha = getFecha(textoFecha);
		Calendar prueba = convertidorDateACalendar(fecha);
		
		
		esfamiliaNumerosa = comprobarEdadPasajero(reader, edad, esfamiliaNumerosa);
		
		System.out.println("Desea billete flexible SI/NO");
		respuestaBilleteFlexible = reader.nextLine();
		tieneBilleteFlexible = comprobarRespuestaUsuario(respuestaBilleteFlexible);
		
		
		
		billete.calcularPrecioFinal(prueba, edad, esfamiliaNumerosa, tieneBilleteFlexible);
		System.out.println(billete.toString());
		
		reader.close();
	}


	private static boolean comprobarEdadPasajero(Scanner reader, int edad, boolean sidofamiliaNumerosa) {
		String respuestaFamiliaNumerosa;
		if(edad > Billete.getEdadMaximaConDescuento()) {
			System.out.println("Es familia numerosa SI/NO");
			respuestaFamiliaNumerosa = reader.nextLine();
			sidofamiliaNumerosa = comprobarRespuestaUsuario(respuestaFamiliaNumerosa);
		}
		return sidofamiliaNumerosa;
	}

	
	private static boolean comprobarRespuestaUsuario(String respuesta) {
		if(respuesta.equals(SI) || respuesta.toLowerCase().equals(SI_MINUSCULA)) {
			return true;
		}else {
			return false;
		}
	}
	
	public static Date getFecha(String fecha) {
		try {
			return new SimpleDateFormat(FORMATO_FECHA).parse(fecha);
		} catch (ParseException e) {
			return null;
		}
	}
	private static Calendar convertidorDateACalendar(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
			return calendar;

	}

}

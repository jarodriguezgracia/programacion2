package practica2;



import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;


public class AgenciaViajes {
	

	private static final int CANTIDAD_ELEMENTOS_EN_COLECCION = 20;
	private Trabajador[] listaTrabajadores = new Trabajador[CANTIDAD_ELEMENTOS_EN_COLECCION];
	private PaqueteViaje[] listaPaquetesViaje = new PaqueteViaje[CANTIDAD_ELEMENTOS_EN_COLECCION];
	private String nombre;
	private static final String FORMATO_FECHA = "dd-MM-yyyy HH:mm";	
	private int posicionListaTrabajadores = 0;
	private int posicionListaPaquetes = 0;
	
	public AgenciaViajes(String nombre) {
		this.nombre = nombre;
	}
		
	public void cambiarDisponibilidadPaquete(Scanner reader) {
		int id = asignarEnteroDeTeclado(reader,"Introduce el ID del paquete");
		
		PaqueteViaje[] listaPaquetes = this.listaPaquetesViaje;
		for (int i = 0; i < listaPaquetes.length; i++) {
			if(listaPaquetes[i] instanceof PaqueteViaje) {
				if(listaPaquetes[i].getId() == id) {
					listaPaquetes[i].cambiarDisponibilidad();
				}
			}
		}
		
	}

	private int asignarEnteroDeTeclado(Scanner reader,String mensaje) {
		System.out.println(mensaje);
		int id = reader.nextInt();
		return id;
	}
	
	public void registrarVentaPaquete(Scanner reader) {
		int id = asignarEnteroDeTeclado(reader,"Introduce el ID del paquete");
		
		PaqueteViaje[] listaPaquetes = this.listaPaquetesViaje;
		for (int i = 0; i < listaPaquetes.length; i++) {
			if(listaPaquetes[i] instanceof PaqueteViaje) {
				if(listaPaquetes[i].getId() == id) {
					listaPaquetes[i].incrementarVentas();
				}
			}
		}
		
	}

	public void consultarPrecioPaquete(Scanner reader) {
		int id = asignarEnteroDeTeclado(reader,"Introduce el ID del paquete");
		
		PaqueteViaje[] listaPaquetes = this.listaPaquetesViaje;
		for (int i = 0; i < listaPaquetes.length; i++) {
			if(listaPaquetes[i] instanceof PaqueteViaje) {
				if(listaPaquetes[i].getId() == id) {
					System.out.println("El precio del paquete es " + listaPaquetes[i].mostrarPrecioFinal());
				}
			}
		}
		
	}
	

	public void mostrarPaquetesDisponibles() {
		for (PaqueteViaje paquete : this.listaPaquetesViaje) {
			if(paquete instanceof PaqueteViaje) {
				if(paquete.estaDisponible() == true) {
					System.out.println(paquete.toString());
				}
			}
		}
		
	}

	public void anadirPaquete(Scanner reader) {
		limpiarBuffer(reader);
		String nombre = asignarCadenaDeTeclado(reader,"Introduce el nombre del paquete");
		int numeroVentas = asignarEnteroDeTeclado(reader, "Introduce la cantidad de ventas del paquete");
		double precioBase = asignarDecimalDeTeclado(reader,"Introduce el precio base");
		limpiarBuffer(reader);
		String textoFecha = asignarCadenaDeTeclado(reader,"Introduce la fecha de incio del viaje con formato dd-MM-yyyy HH:mm");
		Date fecha = Fecha.getFecha(textoFecha,FORMATO_FECHA);
		Calendar fechaInicioViaje = Fecha.cambiarDateACalendar(fecha);
		textoFecha = asignarCadenaDeTeclado(reader,"Introduce la fecha de finalizacion del viaje con formato dd-MM-yyyy HH:mm");
		fecha = Fecha.getFecha(textoFecha,FORMATO_FECHA);
		Calendar fechaFinViaje = Fecha.cambiarDateACalendar(fecha);
		int cantidadResponsables = asignarEnteroDeTeclado(reader, "Cuantos responsables tiene el paquete");
		limpiarBuffer(reader);
		Trabajador[] responsables = asignarResponsablesAPaquete(reader, cantidadResponsables);
		PaqueteViaje nuevoPaquete = new PaqueteViaje(nombre,numeroVentas,precioBase,fechaInicioViaje,fechaFinViaje,responsables);
		PaqueteViaje[] listaPaquetes = this.listaPaquetesViaje;
		listaPaquetes[posicionListaPaquetes] = nuevoPaquete;
		posicionListaPaquetes++;
		
	}

	private Trabajador[] asignarResponsablesAPaquete(Scanner reader, int cantidadResponsables) {
		Trabajador[] responsables = new Trabajador[cantidadResponsables];
		boolean trabajadorEncontrado;
		for (int i = 0; i < cantidadResponsables; i++) {
			String dni = asignarCadenaDeTeclado(reader, "Introduce dni del trabajador responsable");
			trabajadorEncontrado = false;
			for (Trabajador trabajador : this.listaTrabajadores) {
				if(trabajador instanceof Trabajador) {
					if(trabajador.getDNI().getDni().equals(dni)) {
						responsables[i] = trabajador;
						trabajadorEncontrado = true;

					}
				}
			}
			if(trabajadorEncontrado == false) {
				System.out.println("No se ha encontrado el trabajador con DNI "+ dni);
				i--;
			}
			
		}
		return responsables;
	}

	private double asignarDecimalDeTeclado(Scanner reader,String mensaje) {
		System.out.println(mensaje);
		double precioBase = reader.nextDouble();
		return precioBase;
	}

	private String asignarCadenaDeTeclado(Scanner reader,String mensaje) {
		System.out.println(mensaje);
		String nombre = reader.nextLine();
		return nombre;
	}


	private void limpiarBuffer(Scanner reader) {
		reader.nextLine();
		
	}

	public void anadirTrabajador(Scanner reader) {
		limpiarBuffer(reader);
		String nombre = asignarCadenaDeTeclado(reader, "Introduce el nombre del trabajador");
		String dni;
		do {
			dni = asignarCadenaDeTeclado(reader, "Introduce el DNI del trabajador");
			if(DNI.comprobarDNI(dni) == false) {
				System.out.println("Error en el dni introducido");
			}
			if(comprobarDniRepetido(dni)) {
				System.out.println("DNI existente");
			}
		}while(DNI.comprobarDNI(dni) == false || comprobarDniRepetido(dni) == true);
		String direccion = asignarCadenaDeTeclado(reader, "Introduce la direccion del trabajador");
		String iban = asignarCadenaDeTeclado(reader, "Introduce el IBAN del trabajador");
		double sueldo = asignarDecimalDeTeclado(reader, "Introduce el sueldo");
		DNI dniTrabajador = new DNI(dni);
		Trabajador nuevoTrabajador = new Trabajador(nombre,dniTrabajador,direccion,iban,sueldo);
		Trabajador[] listaTrabajadores = this.listaTrabajadores;
		listaTrabajadores[posicionListaTrabajadores] = nuevoTrabajador;
		posicionListaTrabajadores++;
		
	}


	public boolean comprobarDniRepetido(String dni) {
		for (Trabajador trabajador : this.listaTrabajadores) {
			if(trabajador instanceof Trabajador) {
				if(trabajador.getDNI().getDni().equals(dni)) {
					return true;
				}
			}
		}
		return false;
	}

	public void extraerInformacionTrabajador(Scanner reader) {
		limpiarBuffer(reader);
		String dniTrabajador = asignarCadenaDeTeclado(reader, "Introduce el DNI del trabajador");
		boolean encontrado = false;
		
		for (Trabajador trabajador : this.listaTrabajadores) {
			if(trabajador instanceof Trabajador) {
				if(trabajador.getDNI().getDni().equals(dniTrabajador)) {
					System.out.println(trabajador.toString());
					System.out.println("Responsable de paquetes: ");
					for (int i = 0; i < listaPaquetesViaje.length; i++) {
						if(listaPaquetesViaje[i] instanceof PaqueteViaje) {
							Trabajador[] responsables = listaPaquetesViaje[i].getResponsables();
							for (int j = 0; j < responsables.length; j++) {
								Trabajador trabajadorAuxiliar = responsables[j];
								if(trabajadorAuxiliar.equals(trabajador)) {
									System.out.println(listaPaquetesViaje[i] + "\n");
									
								}
							}
						}
					}
					encontrado = true;
				}
			}
		}
		
		if(encontrado == false) {
			System.out.println("No se ha encontrado trabajador");
		}
		
	}


	public void listarTrabajadores() {
		
		if(posicionListaTrabajadores != 0) {
			for (Trabajador trabajador : this.listaTrabajadores) {
				if(trabajador instanceof Trabajador) {
					System.out.println(trabajador.toString());
				}
			}
		}
	}


	public String getNombre() {
		return nombre;
	}
}

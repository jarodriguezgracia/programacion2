package practica2;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Fecha {
	
	
	private static final String DD_MM_YYYY_HH_MM = "dd/MM/yyyy HH:mm";

	public static Date getFecha(String fecha, String formato) {
		try {
			return new SimpleDateFormat(formato).parse(fecha);
		} catch (ParseException e) {
			return null;
		}
	}
	
	public static Calendar cambiarDateACalendar(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
			return calendar;

	}
	
	public static Date cambiarCalendarADate(Calendar calendar) {
		Date date=calendar.getTime();
			return date;

	}
	
	public static String formateaFecha(Calendar fechaViaje){
		Date fecha = cambiarCalendarADate(fechaViaje);
        SimpleDateFormat simpleDateFormat =
                new SimpleDateFormat(DD_MM_YYYY_HH_MM);
        return simpleDateFormat.format(fecha);
    }
}

package practica2;

public class DNI {
	
	private static final int INICIO_POSICION_NUMEROS_DNI = 0;
	private static final int TAMANIO_NUMEROS_DNI = 8;
	private static final int TAMANO_DNI = 9;
	private String dni;
	
	
	public DNI(String dni) {
		this.dni = dni;
	}
	
	public static boolean comprobarDNI(String dni) {
		if(dni.length() != TAMANO_DNI || Character.isLetter(dni.charAt(TAMANIO_NUMEROS_DNI)) == false) {
			return false;
		}
		String letraMayuscula = (dni.substring(TAMANIO_NUMEROS_DNI)).toUpperCase();
		
		if(comprobarNumerosDNI(dni) == true && comprobarLetraDNI(dni).equals(letraMayuscula)) {
			return true;
		}else {
			return false;
		}
	} 

	private static String comprobarLetraDNI(String dni) {
		int parteNumerica = Integer.parseInt(dni.substring(INICIO_POSICION_NUMEROS_DNI,TAMANIO_NUMEROS_DNI));
		int resto = 0;
		String letra;
		String[] asignacionLetra = {"T","R","W","A","G","M","Y","F","P","D","X","B","N","J","Z","S","Q","V","H","L","C","K","E"};
		resto = parteNumerica % 23;
		letra = asignacionLetra[resto];
		return letra;
		
	}
	
	private static boolean comprobarNumerosDNI(String dni) {
		String [] unoNueve = {"0","1","2","3","4","5","6","7","8","9"};
		String numero;
		String dniNumerico = "";
		
		for (int i = 0; i < dni.length() - 1; i++) {
			numero = dni.substring(i,i+1);
			for (int j = 0; j < unoNueve.length; j++) {
				if(numero.equals(unoNueve[j])) {
					dniNumerico += unoNueve[j];
				}
			}
			
		}
		if(dniNumerico.length() != TAMANIO_NUMEROS_DNI) {
			return false;
		}else {
			return true;
		}
		
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	@Override
	public String toString() {
		return  dni ;
	}
	
	
}

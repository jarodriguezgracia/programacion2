package practica2;

import java.util.Scanner;

public class Main {

	private static final int OPCION_SALIR_PROGRAMA_9 = 9;

	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		System.out.println("Introduce nombre de la agencia");
		String nombreAgencia = reader.nextLine();
		AgenciaViajes agencia = new AgenciaViajes(nombreAgencia);
		int opcion;
		do {
			System.out.println("***" + " " + agencia.getNombre() + " " + "MEN� PRINCIPAL ***");
			System.out.println("************* GESTI�N **************");
			System.out.println("1. Ver listado de trabajadores.");
			System.out.println("2. Ver informaci�n sobre un trabajador.");
			System.out.println("3. Dar de alta a un nuevo trabajador.");
			System.out.println("4. Crear paquete de viaje");
			System.out.println("5. Ver paquetes disponibles.");
			System.out.println("6. Modificar disponibilidad paquete.");
			System.out.println("************* OPERATIVA *************");
			System.out.println("7. Ver precio de un viaje.");
			System.out.println("8. Registrar venta de paquete");
			System.out.println("9. Salir");
			System.out.println("*************************************");
			opcion = reader.nextInt();
			
			respuestaUsuario(opcion, agencia, reader);
			
		}while(opcion != OPCION_SALIR_PROGRAMA_9);
		
		
		reader.close();

	}
	
	public static void respuestaUsuario(int opcionUsurario, AgenciaViajes agencia, Scanner reader) {
		
		switch (opcionUsurario) {
		case 1:
			agencia.listarTrabajadores();
			break;
		case 2:
			agencia.extraerInformacionTrabajador(reader);
			break;
		case 3:
			agencia.anadirTrabajador(reader);
			break;
		case 4:
			agencia.anadirPaquete(reader);
			break;
		case 5:
			agencia.mostrarPaquetesDisponibles();
			break;
		case 6:
			agencia.cambiarDisponibilidadPaquete(reader);
			break;
		case 7:
			agencia.consultarPrecioPaquete(reader);
			break;
		case 8:
			agencia.registrarVentaPaquete(reader);
			break;
		case 9:
				System.out.println("Cerrando Programa");
			break;
			
		default:
			System.out.println("Valor introducido desconocido");
			break;
		}
		
	}

}

package practica2;

public class Trabajador {
	
	
	private String nombre;
	private DNI DNI;
	private String direccion;
	private String cuentaBancaria;
	private double sueldo;
	

	public Trabajador(String nombre, DNI dNI, String direccion, String cuentaBancaria, double sueldo) {
		
		this.nombre = nombre;
		this.DNI = dNI;
		this.direccion = direccion;
		this.cuentaBancaria = cuentaBancaria;
		this.sueldo = sueldo;
	}
	
	public DNI getDNI() {
		return DNI;
	}

	@Override
	public String toString() {
		return "Trabajador [nombre=" + nombre + ", DNI=" + DNI.toString() + ", direccion=" + direccion + ", cuentaBancaria="
				+ cuentaBancaria + ", sueldo=" + sueldo + "]";

	}
	


}

package practica2;

import java.util.Calendar;
import java.util.GregorianCalendar;



public class PaqueteViaje {
	
	private static int ID_GLOBAL = 0;
	private int id;
	private String nombre;
	private int cantidadVentas;
	private double precioBase;
	private double precioFinal;
	private boolean disponibilidad;
	private Trabajador[] responsables;
	private Calendar fechaInicioViaje;
	private Calendar fechaFinViaje;
	
	private static double INCREMENTO_TEMPORADA_ALTA = 3;
	private static double DECREMENTO_TEMPORADA_BAJA = 0.2;
	
	private static Calendar FECHA_INICIO_TEMPORADA_ALTA = new GregorianCalendar(2020, Calendar.JULY, 31);
	private static Calendar FECHA_FIN_TEMPORADA_ALTA = new GregorianCalendar(2020, Calendar.SEPTEMBER, 1);
	
	private static Calendar FECHA_INICIO_TEMPORADA_BAJA_PRIMER_PERIODO = new GregorianCalendar(2020, Calendar.AUGUST, 31);
	private static Calendar FECHA_FIN_TEMPORADA_BAJA_PRIMER_PERIODO = new GregorianCalendar(2020, Calendar.DECEMBER, 1);

	private static Calendar FECHA_INICIO_TEMPORADA_BAJA_SEGUNDO_PERIODO = new GregorianCalendar(2020, Calendar.FEBRUARY, 14);
	private static Calendar FECHA_FIN_TEMPORADA_BAJA_SEGUNDO_PERIODO = new GregorianCalendar(2020, Calendar.MARCH, 2);
	

	public PaqueteViaje(String nombre, int numeroVentas, double precioBase, Calendar fechaInicio,
			Calendar fechaFin, Trabajador[] responsables) {
		this.id = PaqueteViaje.ID_GLOBAL++;
		this.nombre = nombre;
		this.cantidadVentas = numeroVentas;
		this.precioBase = precioBase;
		cambiarDisponibilidadTrue();
		this.fechaInicioViaje = fechaInicio;
		this.fechaFinViaje = fechaFin;
		this.responsables = new Trabajador[responsables.length];
	
		for (int i = 0; i < responsables.length; i++) {
			this.responsables[i] = responsables[i];
		}
		this.precioFinal = this.precioBase;
		comprobarFechas();
	
	}



	private void comprobarFechas() {
		if(this.fechaInicioViaje.after(FECHA_INICIO_TEMPORADA_ALTA) && this.fechaInicioViaje.before(FECHA_FIN_TEMPORADA_ALTA)) {
			incrementarPrecioFinal(this.precioBase*PaqueteViaje.INCREMENTO_TEMPORADA_ALTA);
			
		}else if((this.fechaInicioViaje.after(FECHA_INICIO_TEMPORADA_BAJA_PRIMER_PERIODO) && this.fechaInicioViaje.before(FECHA_FIN_TEMPORADA_BAJA_PRIMER_PERIODO)) 
				|| 
				(this.fechaInicioViaje.after(FECHA_INICIO_TEMPORADA_BAJA_SEGUNDO_PERIODO) && this.fechaInicioViaje.before(FECHA_FIN_TEMPORADA_BAJA_SEGUNDO_PERIODO))) {
			decrementarPrecio(this.precioBase*PaqueteViaje.DECREMENTO_TEMPORADA_BAJA);
		}
		
	}

	private void decrementarPrecio(double decremento) {
		this.precioFinal = this.precioBase - decremento;
	}

	private void incrementarPrecioFinal(double incremento) {
		this.precioFinal = this.precioBase + incremento;
	}

	@Override
	public String toString() {
		return "PaqueteViaje: \n id=" + id + ", Nombre=" + nombre + ",\n Cantidad de ventas=" + cantidadVentas + ",\n Precio base="
				+ precioBase + ",\n Precio final=" + precioFinal + ",\n Disponibilidad=" + disponibilidad + 
				"\n Fecha inicio de viaje: " + Fecha.formateaFecha(this.fechaInicioViaje) + "\n Fecha fin de viaje: " +Fecha.formateaFecha(this.fechaFinViaje);
	}
	
	public boolean estaDisponible() {
		return disponibilidad;
	}
	public int getId() {
		return id;
	}
	
	public void cambiarDisponibilidad() {
		if(this.disponibilidad == true) {
			cambiarDisponibilidadFalse();
		}else {
			cambiarDisponibilidadTrue();
		}
	}



	private void cambiarDisponibilidadTrue() {
		this.disponibilidad = true;
	}



	private void cambiarDisponibilidadFalse() {
		this.disponibilidad = false;
	}
	
	public void incrementarVentas() {
		this.cantidadVentas++;
	}

	public double mostrarPrecioFinal() {
		return this.precioFinal;
	}

	public Trabajador[] getResponsables() {
		return responsables;
	}

}

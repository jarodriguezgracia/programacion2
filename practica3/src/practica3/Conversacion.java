package practica3;

import java.util.Scanner;

public class Conversacion {
	private static final int CANTIDAD_USUARIOS_1 = 1;
	private static final int CANTIDAD_USUARIOS_2 = 2;
	private int POSICION_MENSAJES = 0;
	private static int ID_GLOBAL = 0;
	private int id;
	private String titulo;
	private Mensaje[] mensajes;
	private int numeroMensajesNoLeidos = 0;
	private Usuario[] listaUsuarios;
	private int POSICION_USUARIOS = 0;
	
	public Conversacion(String titulo) {
		this.id = Conversacion.ID_GLOBAL;
		Conversacion.ID_GLOBAL++;
		this.titulo = titulo;
		this.numeroMensajesNoLeidos = 0;
		this.mensajes = new Mensaje[50];
		this.listaUsuarios = new Usuario[20];
	}
	
	public void aniadirUsuario(Usuario usuario) {
		boolean respuesta = existeUsarioEnConversacion(usuario);
		if(respuesta) {
			
		}else {
			listaUsuarios[POSICION_USUARIOS] = usuario;
			this.POSICION_USUARIOS++;
		}
	}
	
	private boolean existeUsarioEnConversacion(Usuario usuario) {
		for (int i = 0; i < listaUsuarios.length; i++) {
			if(listaUsuarios[i] != null) {
				if(usuario.equals(listaUsuarios[i])) {
					return true;
				}
			}
		}
		return false;
	}

	public void aniadirMensaje(Mensaje mensaje) {
		this.mensajes[POSICION_MENSAJES] = mensaje;
		this.POSICION_MENSAJES++;
		this.numeroMensajesNoLeidos++;
	}
	public void aniadirMensajePropio(Mensaje mensaje) {
		this.mensajes[POSICION_MENSAJES] = mensaje;
		this.POSICION_MENSAJES++;
	}


	@Override
	public String toString() {
		return  id + "____" + titulo + "____" + this.mensajes[POSICION_MENSAJES-1].toString() + "____" + this.numeroMensajesNoLeidos;
	}

	
	public boolean compararId(int id) {
		if(this.id == id) {
			return true;
		}else {
			return false;
		}
	}

	

	public void mostrarMensajes(Usuario usuario) {
		for (int i = 0; i < mensajes.length; i++) {
			if(mensajes[i] != null) {
				if(this.POSICION_USUARIOS == CANTIDAD_USUARIOS_2 || this.POSICION_USUARIOS == CANTIDAD_USUARIOS_1) {
					if(usuario.compararId(mensajes[i].getIdUsuario())) {
						System.out.println(i + "____" + usuario.mostrarNombre() + "____" + listaUsuarios[0].mostrarNombre() + " " + mensajes[i].toString());
					}else {
						System.out.println(i + "____" + listaUsuarios[0].mostrarNombre()  + "____" + usuario.mostrarNombre() + " " + mensajes[i].toString());

					}
					
				}else {
					if(usuario.compararId(mensajes[i].getIdUsuario())) {
						System.out.println(i + "____" + usuario.mostrarNombre() + "____" + this.titulo +  " " + mensajes[i].toString());
					}else {
						System.out.println(i  + "____" + this.titulo + "____" + usuario.mostrarNombre() + " " + mensajes[i].toString());

					}
				}
			}
		}
		this.numeroMensajesNoLeidos = 0;
		
	}

	public Mensaje crearMensajeDesdeConversacion(Usuario usuario) {
		Mensaje mensaje = null;
		@SuppressWarnings("resource")
		Scanner reader = new Scanner(System.in);
		for (int i = 0; i < listaUsuarios.length; i++) {
			if(listaUsuarios[i] != null) {
				if(!listaUsuarios[i].equals(usuario)) {
					String texto = reader.nextLine();
					mensaje = new Mensaje(texto,this.listaUsuarios[i].getId());
					return mensaje;
				}
			}
			
		}
	
		return mensaje;
	}

	



	
		
	

}

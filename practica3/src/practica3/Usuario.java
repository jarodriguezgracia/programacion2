package practica3;

public class Usuario {
	private static int ID_GLOBAL = 0;

	private String nombre;
	private int id;

	public Usuario(String nombre) {
		this.nombre = nombre;
		this.id = Usuario.ID_GLOBAL;
		Usuario.ID_GLOBAL++;
	}

	public String mostrarNombre() {
		return nombre;
	}
	
	/**
	 * obligatoria para simular mensajes de otros users y crear mensajes para no ocupar mas memoria teniendo un usuario asignado a mensaje
	 * @return id
	 */
	public int getId() {
		return id;
	}

	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Usuario other = (Usuario) obj;
		if (id != other.id)
			return false;
		if (nombre == null) {
			if (other.nombre != null)
				return false;
		} else if (!nombre.equals(other.nombre))
			return false;
		return true;
	}
	
	public boolean compararId(int id) {
		if(this.id == id) {
			return true;
		}else {
			return false;
		}
	}
	
	

}

package practica3;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Scanner;

public class Chat {
	private static int POSICION_CONVERSACIONES = 0;
	private Usuario usuario;
	private Conversacion[] listaConversaciones;
	
	
	public Chat (Usuario usuario) {
		this.usuario = usuario;
		this.listaConversaciones = new Conversacion[20];
		predeclararConversacionIndividual();
		predeclararConversacionGrupal();
	}


	private void predeclararConversacionGrupal() {
		
		Conversacion conversacionGrupal = new Conversacion("Alumnos de programacion II");
		
		Usuario user1 =  new Usuario("pepe");
		Usuario user2 = new Usuario("Franchesco");
		conversacionGrupal.aniadirUsuario(user1);
		conversacionGrupal.aniadirUsuario(user2);
		//cambiarlo p�ra la demostracion
		Calendar FECHA = new GregorianCalendar(2020, Calendar.MARCH, 20);
		Mensaje ultimoMensaje = new Mensaje("Vaya pr�ctica m�s chula",user1.getId(),FECHA);
		conversacionGrupal.aniadirMensaje(ultimoMensaje);
		listaConversaciones[POSICION_CONVERSACIONES] = conversacionGrupal;
		Chat.POSICION_CONVERSACIONES++;
	}


	private void predeclararConversacionIndividual() {
		Conversacion conversacionIndividual = new Conversacion("Alumno que nunca viene");
		Usuario user1 = new Usuario("Alumno que nunca viene");
		conversacionIndividual.aniadirUsuario(user1);
		Mensaje ultimoMensaje = new Mensaje("�que es un objeto?",user1.getId());
		conversacionIndividual.aniadirMensaje(ultimoMensaje);
		listaConversaciones[POSICION_CONVERSACIONES] = conversacionIndividual;
		Chat.POSICION_CONVERSACIONES++;
		
		Conversacion conversacionIndividual2 = new Conversacion("Iv�n Verde");
		
		Usuario user2 = new Usuario("Iv�n Verde");
		Mensaje ultimoMensaje2 = new Mensaje("No es my dificil",user2.getId());
		conversacionIndividual2.aniadirMensaje(ultimoMensaje2);
		conversacionIndividual2.aniadirUsuario(user2);
		listaConversaciones[POSICION_CONVERSACIONES] = conversacionIndividual2;
		Chat.POSICION_CONVERSACIONES++;
		
		Conversacion conversacionIndividual3 = new Conversacion("Alumno aventajado");
		Usuario user3 = new Usuario("Alumno aventajado");
		conversacionIndividual2.aniadirUsuario(user3);
		Mensaje ultimoMensaje3 = new Mensaje("Yo ya la he terminado",user3.getId());
		conversacionIndividual3.aniadirMensaje(ultimoMensaje3);
		listaConversaciones[POSICION_CONVERSACIONES] = conversacionIndividual3;
		Chat.POSICION_CONVERSACIONES++;
	}


	public void mostrarConversaciones() {
		
		for (int i = 0; i < listaConversaciones.length; i++) {
			if(listaConversaciones[i] != null){
				System.out.println(listaConversaciones[i].toString());
			}
		}
		
		
	}


	public void mostrarConversacion(int id) {
		for (int i = 0; i < listaConversaciones.length; i++) {
			if(listaConversaciones[i] != null) {
				if(listaConversaciones[i].compararId(id)) {
					listaConversaciones[i].mostrarMensajes(this.usuario);
				}
			}
		}
		
	}


	public void enviarMensaje(int id) {
		Mensaje mensaje = crearMensaje();
		for (int i = 0; i < listaConversaciones.length; i++) {
			if(listaConversaciones[i] != null) {
				if(listaConversaciones[i].compararId(id)) {
					listaConversaciones[i].aniadirMensajePropio(mensaje);
					listaConversaciones[i].aniadirUsuario(usuario);
				}
			}
		}
		
	}


	private Mensaje crearMensaje() {
		@SuppressWarnings("resource")
		Scanner reader = new Scanner(System.in);
		String texto = reader.nextLine();
		Mensaje mensaje = new Mensaje(texto,this.usuario.getId());
		return mensaje;
	}


	public void simularEnvio(int id) {
		
		for (int i = 0; i < listaConversaciones.length; i++) {
			if(listaConversaciones[i] != null) {
				if(listaConversaciones[i].compararId(id)) {
					Mensaje mensaje = listaConversaciones[i].crearMensajeDesdeConversacion(this.usuario);
					listaConversaciones[i].aniadirMensaje(mensaje);
					listaConversaciones[i].aniadirUsuario(usuario);
				}
			}
		}
		
	}

}

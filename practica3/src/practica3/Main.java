package practica3;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		
		Usuario usuario = new Usuario("yo");
		Chat chat = new Chat(usuario);
		do {
			chat.mostrarConversaciones();
			int id = pedirIDConversacion(reader);

			int opcion;
			do {
				System.out.println("************* OPCIONES **************");
				System.out.println("1. Ver mensaje.");
				System.out.println("2. Enviar mensaje.");
				System.out.println("3. Simular recepci�n de mensaje.");
				System.out.println("4. Volver atr�s.");
				System.out.println("*************************************");
				opcion = reader.nextInt();
				respuestaUsuario(opcion,chat,id);
			}while(opcion != 4);
		}while(true);
	}

	private static void respuestaUsuario(int opcion, Chat chat, int id) {
		switch (opcion) {
		case 1:
				chat.mostrarConversacion(id);
			break;
		case 2:
				chat.enviarMensaje(id);
			break;
		case 3:
				chat.simularEnvio(id);
			break;
		default:
			break;
		}
	}

	private static int pedirIDConversacion(Scanner reader) {
		System.out.println("Introduzca ID de conversacion");
		int id = reader.nextInt();
		return id;
	}

}

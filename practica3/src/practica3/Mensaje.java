package practica3;

import java.util.Calendar;


public class Mensaje {
		
		private String mensaje;
		private Calendar fecha;
		private int idUsuario;
		
		
		public Mensaje(String mensaje,int id) {
			this.mensaje = mensaje;
			this.idUsuario = id;
			this.fecha = Calendar.getInstance();
	}
	public Mensaje(String mensaje,int id,Calendar fecha) {
			this.mensaje = mensaje;
			this.idUsuario = id;
			this.fecha = fecha;
	}

		@Override
		public String toString() {
			return mensaje + "____"+ comprobarFecha();
		}

		private String comprobarFecha() {
			if(this.fecha.get(Calendar.DAY_OF_MONTH) == Calendar.getInstance().get(Calendar.DAY_OF_MONTH) && 
					this.fecha.get(Calendar.MONTH) == Calendar.getInstance().get(Calendar.MONTH) && this.fecha.get(Calendar.YEAR) == Calendar.getInstance().get(Calendar.YEAR)
					) {
				return fecha.get(Calendar.HOUR_OF_DAY) + ":" +fecha.get(Calendar.MINUTE);
			}else if(this.fecha.get(Calendar.DAY_OF_MONTH) == (Calendar.getInstance().get(Calendar.DAY_OF_MONTH)-1) && 
					this.fecha.get(Calendar.MONTH) == Calendar.getInstance().get(Calendar.MONTH) && this.fecha.get(Calendar.YEAR) == Calendar.getInstance().get(Calendar.YEAR)
					) {
				return "Ayer";
			}else {
				return Fecha.formateaFecha(this.fecha);
			}
			
		}
		/**
		 * obligatorio para comparar en conversacion y poder operar con el
		 * @return
		 */
		public int getIdUsuario() {
			return idUsuario;
		}

		


	
		
		

		
		
}
